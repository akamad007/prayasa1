from django.conf.urls import patterns, include, url
from django.contrib import admin
from settings import OUR_APPS
import settings 

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'amar.views.wikipedia', name='wikipedia'),
     url(r'^$', include('wikipedia.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
     url(r'^search/', include('haystack.urls')),
)
for apps in OUR_APPS:
    urlpatterns += patterns('',      
     url(r'^'+apps+'/', include(apps+'.urls')),
    )
urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))


from django import template
template.add_to_builtins('wikipedia.templatetags.search')