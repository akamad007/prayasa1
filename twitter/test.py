import pandas as pd
import nltk

df = pd.read_csv("medium.csv", sep=",")
neg_tweets = []
pos_tweets = []
neutral_tweets = []
i=0
for row in df.itertuples():
    index, a, b, c, d, e, f  = row
    
    if float(a)>2:
        pos_tweets.append((f,a))
    elif (float(a)==2):
        neutral_tweets.append((f,a))
    elif float(a)<2:
        neg_tweets.append((f,a))
    i=i+1

tweets = []

for (words, sentiment) in pos_tweets + neg_tweets+neutral_tweets:
    words_filtered = [e.lower() for e in words.split() if len(e) >= 3]
    tweets.append((words_filtered, sentiment))


def get_words_in_tweets(tweets):
    all_words = []
    for (words, sentiment) in tweets:
        all_words.extend(words)
    return all_words
    
def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features
    
word_features = get_word_features(get_words_in_tweets(tweets))

def extract_features(document):
    document_words = set(document)
    features = {}
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features
    
training_set = nltk.classify.apply_features(extract_features, tweets)
classifier = nltk.NaiveBayesClassifier.train(training_set)

import pickle
f = open('First_classifier.pickle', 'wb')
pickle.dump(classifier, f)
f.close()

df = pd.read_csv("small.csv", sep=",")

for row in df.itertuples():
    index, a, b, c, d, e, f  = row
    print classifier.classify(extract_features(f.split()))


