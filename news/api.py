from haystack.query import SearchQuerySet
from settings import CONNECTIONS
from news.models import NewsArticle
import textwrap
from haystack.inputs import AltParser
    
def getSolrNewsArticles(query,title):
    doclist = []
    i = 0
    titleSplit = title.split(' ')
    for t in titleSplit:
        test = textwrap.wrap(t, 5)
        for t1 in test: 
            doclist = doclist + SearchQuerySet().using(CONNECTIONS[1]).filter(headline__auto=t1).models(NewsArticle)[:2]
   
   
    testlist = []
    for q in query:
        i +=1
        if i>4:
            break       
        testlist = doclist + SearchQuerySet().using(CONNECTIONS[1]).filter(content = q).models(NewsArticle)[0:1] 
    if not testlist:
        testlist = SearchQuerySet().using(CONNECTIONS[1]).filter(content=AltParser('dismax', title, qf='headline', mm=1))[0:3]
    
    doclist = testlist+doclist
    doclist = doclist[:3]
    return doclist

def getSolrNewsArticle(pk):
    sqs = SearchQuerySet().using(CONNECTIONS[1]).filter(django_id = int(pk)).models(NewsArticle)   
    return sqs