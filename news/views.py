# Create your views here.

import xml.etree.ElementTree as ET
import glob
import datetime

from news.models import NewsArticle


def store(request):
    file_list = glob.glob("E:\\Studies PDF Sem 1\\CSE - 535 Information Retrieval\\Project\\Proj3Data\\TestFlatten\\*.xml")
   
    for doc in file_list:
        
        doc = ET.parse(doc)
        root = doc.getroot()
        
        head = root.find('head')
        pubdate =  head.find('pubdata').get('date.publication')
        body = root.find('body')
        
        leadparagraph = ""
        fulltext = ""
        byline = ""
        
        copyright = head.find('pubdata').get('name')
        onlineurl = head.find('pubdata').get('ex-ref')
        
        pubdate = pubdate.replace("T","")
        pubdate = datetime.datetime.strptime(pubdate, '%Y%m%d%H%M%S')
        
       
        try:
            headline =  ET.tostring(body.find('body.head').find('hedline').find('hl1')).split('<hl1>')[1].split('</hl1>')[0]
        except:
            headline = None
        try:
            abstract =  ET.tostring(body.find('body.head').find('abstract')).split('<abstract>')[1].split('</abstract>')[0]
        except:
            abstract = ""
       
        for block in body.find('body.content').findall('block'):
            if block.attrib.get('class')=="lead_paragraph":
                leadparagraph = ET.tostring(block.find('p')).split('<p>')[1].split('</p>')[0]
            if block.attrib.get('class')=="full_text":
                fulltext = ET.tostring(block.find('p')).split('<p>')[1].split('</p>')[0]
        for by in body.find('body.head').findall('byline'):
            if by.get('class')=="print_byline":
                byline = ET.tostring(by)
        NewsArticle.objects.create(onlineurl=onlineurl,copyright=copyright,leadparagraph=leadparagraph,fulltext=fulltext,byline=byline,abstract=abstract,headline=headline,pub_date = pubdate)
