import datetime
from haystack import indexes
from news.models import NewsArticle


class NewsArticleIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
 
    headline = indexes.EdgeNgramField(model_attr = 'headline')
    copyright = indexes.CharField(model_attr = 'copyright')
    abstract =indexes.CharField(model_attr = 'abstract')
    byline = indexes.CharField(model_attr = 'byline')
    leadparagraph = indexes.CharField(model_attr = 'leadparagraph')
    fulltext = indexes.CharField(model_attr = 'fulltext')
    onlineurl = indexes.CharField(model_attr = 'onlineurl')
    pub_date = indexes.DateTimeField(model_attr='pub_date')

    def get_model(self):
        return NewsArticle

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(pub_date__lte=datetime.datetime.now())
