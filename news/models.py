from django.db import models

# Create your models here.


class NewsArticle(models.Model):
    copyright = models.CharField(max_length=300)
    headline = models.TextField(null=True,blank=True)
    abstract = models.TextField(null=True,blank=True)
    byline = models.CharField(max_length=300)
    leadparagraph = models.TextField(null=True,blank=True)
    fulltext = models.TextField(null=True,blank=True)
    onlineurl = models.URLField(null=True,blank=True)
    pub_date = models.DateTimeField()
    def __unicode__(self):
        return self.headline

    