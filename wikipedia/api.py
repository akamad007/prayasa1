from haystack.query import SearchQuerySet
from settings import CONNECTIONS
from wikipedia.models import WikipediaArticle 
import unicodedata
import requests
from settings import CORE0

baseUrl=CORE0+'/clustering'

def getSolrClusterArticles(query):
   
    payload = {'q':query,'fq':'django_ct:(wikipedia.wikipediaarticle)','wt':'json'}
    r = requests.get(baseUrl, params=payload)
   
    data = r.json()
    cluster = {}
    
    for x in data['clusters']:
        
        docList = []
        for docs in x['docs']:
            for obj in data['response']['docs']:
                if isinstance(docs, unicode):
                    docs = unicodedata.normalize('NFKD', docs).encode('ascii','ignore')
                
                if obj['django_id']==docs.replace('wikipedia.wikipediaarticle.',''):
                                                        docList.append(obj)
        cluster[x['labels'][0]] = docList
    return cluster
    
def getSolrWikiArticles(query):
    sqs = SearchQuerySet().using(CONNECTIONS[0]).filter(content=query).models(WikipediaArticle)[:50]
    
    if len(sqs)<10:
        spellSuggest = SearchQuerySet().using(CONNECTIONS[0]).filter(content=query).models(WikipediaArticle).spelling_suggestion()
        if spellSuggest:
                    spellSuggest =  spellSuggest.replace('(','').replace(')','')  
    else:
        spellSuggest = None    
    return [sqs,spellSuggest]

import re
_digits = re.compile('\d')
def contains_digits(d):
    return not bool(_digits.search(d))

def getSolrWikiArticle(pk):
    sqs = SearchQuerySet().using(CONNECTIONS[0]).filter(django_id = int(pk)).models(WikipediaArticle).facet('fulltext')
    terms = []
    for x in  sqs.facet_counts()['fields']['fulltext']:       
        if (len(x[0])>5) & (contains_digits(x[0])):           
            terms.append(x[0])
    return [sqs,terms]
