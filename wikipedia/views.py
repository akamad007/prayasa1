# Create your views here.
from django.shortcuts import render,redirect,Http404
from django.core.paginator import Paginator, InvalidPage
import datetime

from wikipedia.models import WikipediaArticle
from wikipedia.api import getSolrWikiArticles,getSolrWikiArticle,getSolrClusterArticles
from youtube.api import youTubeSearch
from reddit.api import getRedditData
from flickr.api import getFlickrPics
from news.api import getSolrNewsArticles
from settings import results_per_page

def wikipedia(request):
    
    return render(request,'wikipedia/wikipedia.html')

from haystack.utils import Highlighter
def search(request):
    if(request.method=="GET"):
        searchTerm =  request.GET['search']
        cluster = getSolrClusterArticles(searchTerm)
        articlesSpell = getSolrWikiArticles(searchTerm) 
        articles = articlesSpell[0]
        (paginator, page) = build_page(request,articles)
        highlight = Highlighter(searchTerm)
        for article in page.object_list:
            article.title = highlight.highlight(article.title)
            article.fulltext = highlight.highlight(article.fulltext)
            print article.fulltext
        spellSuggest = articlesSpell[1]  
        error = False
        if len(articles)<1:
            error = True
        
        return render(request,'wikipedia/articles.html',{ 'error':error,'page':page,'paginator':paginator,
                                                         'searchTerm':searchTerm,'articles':articles,
                                                         'cluster':cluster,'spellSuggest':spellSuggest})
    else:
        return redirect("/wikipedia/")


def article(request,article_id):
    if request.method=="GET":
        count = 1
        per_page = 1
        searchTerm = request.GET['search']
        article_list = getSolrWikiArticle(article_id)
        news_list = getSolrNewsArticles(article_list[1],article_list[0][0].title)    
        article =article_list[0][0]
        videos = youTubeSearch(article.title,count)
        searchRedditData = getRedditData(article.title)
        flickrPics = getFlickrPics(article.title,per_page) 
        
      
    else:
        return redirect("/wikipedia/")
    return render(request,'wikipedia/article.html',{'videos':videos,'news_list':news_list,'article':article,'searchTerm':searchTerm,
                                                    'searchRedditData':searchRedditData,'flickrPics':flickrPics,
                                                   'arricle_list':article_list
                                                    })
    
def build_page(request,results):   
        try:
            page_no = int(request.GET.get('page', 1))
        except (TypeError, ValueError):
            raise Http404("Not a valid number for page.")

        if page_no < 1:
            raise Http404("Pages should be 1 or greater.")

        start_offset = (page_no - 1) * results_per_page
        results[start_offset:start_offset + results_per_page]

        paginator = Paginator(results,results_per_page)

        try:
            page = paginator.page(page_no)
        except InvalidPage:
            raise Http404("No such page!")
        return (paginator, page)

    
def removeBrackets(request):
    wikipedia = WikipediaArticle.objects.all()[5000:10000]
    for art in wikipedia:
        
        art.fulltext = art.fulltext.replace('{{','').replace('}}','').replace('[[','').replace(']]','').replace('|',' ').replace('## ',' ').replace('<tt>','').replace('</tt>','')
        art.save()
        
    
from lxml import etree
import unicodedata

def createWikiIndex(request):
        doc_list = 'E:\\Studies PDF Sem 1\\CSE - 535 Information Retrieval\\Project\\Proj3Data\\test.xml'
        parser = etree.XMLParser(recover=True,encoding="UTF-8")
        tree = etree.parse(doc_list, parser)
        root = tree.getroot()
        title = ""
        fulltext = ""
        pubdate= ""
        author = ""
        
        for page in root:             
            if page.tag=="{http://www.mediawiki.org/xml/export-0.9/}page":
                for con in page:                    
                    
                    if con.tag=="{http://www.mediawiki.org/xml/export-0.9/}redirect":
                        title = con.get('title')
                        if isinstance(title, unicode):
                            title = unicodedata.normalize('NFKD', title).encode('ascii','ignore')
                        
                    
                    if con.tag=="{http://www.mediawiki.org/xml/export-0.9/}revision":
                        for data in con:
                            if data.tag=="{http://www.mediawiki.org/xml/export-0.9/}text":
                                if isinstance(data.text, unicode):
                                    data.text = unicodedata.normalize('NFKD', data.text).encode('ascii','ignore')
                                fulltext =  data.text
                                
                            if data.tag=="{http://www.mediawiki.org/xml/export-0.9/}timestamp":
                                if isinstance(data.text, unicode):
                                    data.text = unicodedata.normalize('NFKD', data.text).encode('ascii','ignore')
                                pubdate =  data.text
                                pubdate = pubdate.replace("T", "").replace("Z", "")
                                pubdate = datetime.datetime.strptime(pubdate,'%Y-%m-%d%H:%M:%S')
                            if data.tag=="{http://www.mediawiki.org/xml/export-0.9/}contributor":
                                for contributor in data:
                                    if contributor.tag=="{http://www.mediawiki.org/xml/export-0.9/}username":
                                        if isinstance(contributor.text, unicode):
                                            contributor.text = unicodedata.normalize('NFKD', contributor.text).encode('ascii','ignore')    
                                        
                                        author =  contributor.text
                
                WikipediaArticle.objects.create(title=title,author=author,fulltext=fulltext,pub_date=pubdate)