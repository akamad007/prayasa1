from django.conf.urls import url,patterns


urlpatterns = patterns('wikipedia.views',
    # Examples:
    url(r'^$', 'wikipedia'),
    url(r'^search/$', 'search'),
    url(r'^article/(\w+)/$', 'article'),
    url('^create/$', 'createWikiIndex'),
    url('^remove/$','removeBrackets'),
   
         
   #  url(r'^$', include('wikipedia.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     #url(r'^admin/', include(admin.site.urls)),
)
