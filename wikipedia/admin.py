from django.contrib import admin
from wikipedia.models import WikipediaArticle

admin.site.register(WikipediaArticle)