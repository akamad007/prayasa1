from django.db import models




class WikipediaArticle(models.Model):
    title = models.TextField()
    fulltext = models.TextField()    
    pub_date = models.DateTimeField()
    author = models.TextField()
    
    def __unicode__(self):
        return self.title