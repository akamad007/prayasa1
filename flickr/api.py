'''
Created on Nov 13, 2014

@author: akash
'''

from flickr.flickr_test import photos_search


def getFlickrPics(searchTerm,per_page):
    picsList =  photos_search(tags=searchTerm,per_page =per_page
                 )
    return picsList
    