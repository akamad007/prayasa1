from django.db import models

# Create your models here.
class Reddit():
    title = models.TextField()
    url = models.URLField()
    text = models.TextField()
    author = models.TextField()