import praw
from reddit.models import Reddit

def getRedditData(termSearch):
    r = praw.Reddit(user_agent='my_cool_application')
    r = praw.Reddit('Search post info example by u/_Daimon')
    
    submissions = r.search(termSearch, subreddit=None, sort="new", syntax=None, period="month")
    searchRedditData = []
    i = 0
    for sub in submissions:
        #print sub.author
        reddit = Reddit()
        reddit.title = sub.title
        reddit.text = sub.selftext
        reddit.author = sub.author
        reddit.url = sub.url
        searchRedditData.append(reddit) 
        i = i+1
        if i>2:
            break
    return searchRedditData
     
   