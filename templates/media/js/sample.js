﻿
$(function () {
	var dataI = $('#serial').val();
	var serialData = JSON.parse(dataI);
	var current = $('#current').val();
	
	
	current =  JSON.parse(current);
	
    var data = [];
	for(var i in serialData)
		data.push({name: i, series: serialData[i]});
    plot(data);var currentData = [];
	
	for(var i in current)
		currentData.push({name:i,data:[current[i]]});
	plotBar(currentData);
	
	
	
});

function plot(data) {
    var dataSeries = [];
    for (var i = 0; i < data.length; i++) {
        dataSeries.push({ name: data[i].name, data: data[i].series, decimalValues: 0 });
    }
    $('#chartContainer').highcharts('StockChart', {
    	chart: {
			zoomType: 'x'
		},
        title : {
            text: 'Tweet Trends'
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>'
        },
        yAxis: {
            labels: {
                formatter: function () {
                	var color = this.value >= 0 ? 'green' : 'red'; 
                    return '<span style="color: ' +color + '">'+this.value + '</span>';
                }
            },
            plotLines: [{
                value: 0,
                width: 2,
                color: 'silver'
            }]
        },
        rangeSelector: {
			selected: 0
        },
        series: dataSeries
    });

}
	
function plotBar(data) {
        $('#barChartContainer').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Popularity Rating'
            },
            xAxis: {
                categories: [
                    'Current Trend'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Popularity'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: data
        });
}

